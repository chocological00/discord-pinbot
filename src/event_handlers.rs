use std::{ cmp::Ordering, error::Error };
use serenity::
{
	prelude::*,
	async_trait,
	http::client::Http,
	model::
	{
		id::{ ChannelId, MessageId, GuildId, RoleId },
		user::User,
		guild::{ Guild, Role },
		channel::{ Message, GuildChannel, ChannelType, Reaction, ReactionType },
		gateway::{ Ready, Activity },
	},
	utils::{ MessageBuilder, Color },
	Error as SerenityError
};

use super::database_handler::{ DatabaseHandler, DBError };

const REACTION_EMOJI: &str = "📌";
const REACTION_EMOJI_IN_PLAINTEXT: &str = "pushpin";
const THRESHOLD: u64 = 3;

macro_rules! unwrap_or_return
{
	($e: expr) =>
	{
		match $e
		{
			Ok(x) => x,
			_ => return
		}
	}
}

pub struct Handler
{
	db: DatabaseHandler,
	motd: Option<String>
}

impl Handler
{
	pub fn new(db: DatabaseHandler, motd: Option<String>) -> Handler
	{
		Handler
		{
			db,
			motd
		}
	}
}

#[async_trait]
impl EventHandler for Handler
{
	async fn guild_create(&self, ctx: Context, guild: Guild, is_new: bool)
	{
		// only run routine if the bot is newly invited
		if !is_new { return }

		// delete guild data if it already exists
		self.db.delete_guild(&guild.id).ok();

		let user_info = ctx.cache.current_user().await;

		// send greetings when joined
		let channel_id = match find_default_writable_channel_id(&ctx, &guild).await
		{
			Some(id) => id,
			_ => 
			{
				if let Ok(user) = guild.owner_id.to_user(&ctx.http).await
				{
					let _ = user.dm(&ctx.http, |m|
					{
						m.content(MessageBuilder::new()
							.push("Hello, I couldn't find any writable channel on ")
							.push_bold(&guild.name)
							.push(" server, so I'm sending the intro in DM!")
							.build());

						// TODO: reuse this? not possible on current version of rust afaik
						m.embed(|e|
						{
							let avatar_url = match user_info.avatar_url()
							{
								Some(s) => s,
								_ => user_info.default_avatar_url()
							};

							e.author(|a|
							{
								a.icon_url(&avatar_url);
								a.name(&user_info.name)
							});
							e.color(Color::RED);
							e.title("Hi, I'm Pinbot!");
							e.description("*A bot for Discord servers where 50 pins limit is just not enough.*");
							e.thumbnail(&avatar_url);
							e.field(
								"Getting Started", 
								MessageBuilder::new()
									.push("1. Please create a dedicated pin storage channel that the bot can read/write/manage.\n 2. Then simply call command **\"")
									.mention(&user_info.id)
									.push(" bind\"**!")
									.build(),
								false
							);
							e.field(
								"How to Use",
								format!("{}\n{}\n{}\n{}",
									"After Pinbot is bound to a channel, it starts monitoring all the reactions it can read.",
									format!("When someone posts a message that is pin-worthy, simply react with {} (`:{}:`) emoji!", REACTION_EMOJI, REACTION_EMOJI_IN_PLAINTEXT),
									format!("If the number of {} reaction exceeds certain threshold, it will be *pinned* to the channel Pinbot is bound to!", REACTION_EMOJI),
									"Note: Deleted messages will NOT be unpinned because that is *funnier*. Ask the server admin if it needs to be deleted."
								),
								false
							);
							e.field(
								"Support the Development",
								"Pinbot is written in Rust using Serenity library.\n Feel free to throw a [PR](https://gitlab.com/chocological00/discord-pinbot)!",
								false
							)
						})
					}).await
					.or_else(|e|
					{
						eprintln!("THIS IS NOT A CRASH: An error has occured while sending greetings message as DM");
						eprintln!("Debug info: {:?}", e);
						Err(e)
					});
				}
				return;
			}
		};

		let _ = channel_id.send_message(&ctx.http, |m| 
		{ 
			m.embed(|e|
			{
				let avatar_url = match user_info.avatar_url()
				{
					Some(s) => s,
					_ => user_info.default_avatar_url()
				};

				e.author(|a|
				{
					a.icon_url(&avatar_url);
					a.name(&user_info.name)
				});
				e.color(Color::RED);
				e.title("Hi, I'm Pinbot!");
				e.description("*A bot for Discord servers where 50 pins limit is just not enough.*");
				e.thumbnail(&avatar_url);
				e.field(
					"Getting Started", 
					MessageBuilder::new()
						.push("1. Please create a dedicated pin storage channel that the bot can read/write/manage.\n 2. Then simply call command **\"")
						.mention(&user_info.id)
						.push(" bind\"**!")
						.build(),
					false
				);
				e.field(
					"How to Use",
					format!("{}\n{}\n{}\n{}",
						"After Pinbot is bound to a channel, it starts monitoring all the reactions it can read.",
						format!("When someone posts a message that is pin-worthy, simply react with {} (`:{}:`) emoji!", REACTION_EMOJI, REACTION_EMOJI_IN_PLAINTEXT),
						format!("If the number of {} reaction exceeds certain threshold, it will be *pinned* to the channel Pinbot is bound to!", REACTION_EMOJI),
						"Note: Deleted messages will NOT be unpinned because that is *funnier*. Ask the server admin if it needs to be deleted."
					),
					false
				);
				e.field(
					"Support the Development",
					"Pinbot is written in Rust using Serenity library.\n Feel free to throw a [PR](https://gitlab.com/chocological00/discord-pinbot)!",
					false
				)
			})
		}).await
		.or_else(|e| 
		{
			eprintln!("THIS IS NOT A CRASH: An error has occured while sending greetings message");
			eprintln!("Debug info: {:?}", e);
			Err(e)
		});
	}

	async fn message(&self, ctx: Context, msg: Message)
	{
		// ignore if bot
		if msg.author.bot { return }

		// ignore if not mentioned
		// http only used when cache is disabled
		if !unwrap_or_return!(msg.mentions_me(&ctx.http).await) { return }

		// ignore if message does not include keyword
		if !msg.content.to_ascii_lowercase().contains("bind") { return }

		// check constraints (i.e. message has associated channel, guild, and is writable)
		if let Some(channel) = msg.channel(&ctx.cache).await
		{
			if let Some(c) = channel.guild() // check if channel's in Guild (not DM)
			{
				if let Ok(true) = is_channel_writable(&ctx, &c).await {}
				else { return } // catches everything else, like Err(_) or Ok(false)
			}
		}

		// get guild_id
		let guild_id = if let Some(g) = msg.guild_id
		{
			g
		}
		else { return };

		// try to add to DB. If inserting fails due to duplicate key
		if let Err(DBError::ConstraintViolation) = self.db.bind_guild_channel(&guild_id, &msg.channel_id)
		{
			let bound_channel_id = if let Ok(o) = self.db.get_channel_id(&guild_id)
			{
				o.unwrap()
			}
			else { return };

			let _ = msg.channel_id.say(&ctx.http,
				MessageBuilder::new()
				.push("❌ | The bot is already bound to ")
				.mention(&bound_channel_id)
				.push(" channel. Kick and re-invite to override.")
				.build()
			).await
			.or_else(|e|
			{
				eprintln!("THIS IS NOT A CRASH: An error has occured while sending bind failure message.");
				eprintln!("Debug info: {:?}", e);
				Err(e)
			});

			return;
		}

		// send confirmation message
		let _ = msg.channel_id.say(&ctx.http,
			MessageBuilder::new()
				.push(format!("{} | Channel ", REACTION_EMOJI))
				.mention(&msg.channel_id)
				.push(" is now for pins")
				.build()
		).await
		.or_else(|e|
		{
			eprintln!("THIS IS NOT A CRASH: An error has occured while sending message to new pin channel.");
			eprintln!("Debug info: {:?}", e);
			Err(e)
		});
	}

	async fn reaction_add(&self, ctx: Context, add_reaction: Reaction)
	{
		// ignore if guild DNE
		let guild_id = if let Some(g) = add_reaction.guild_id
		{
			g
		}
		else { return };

		// ignore on emoji mismatch
		match &add_reaction.emoji
		{
			ReactionType::Unicode(s) => 
			{
				if !s.eq(REACTION_EMOJI) { return }
			},
			_ => return
		}

		// ignore if pin channel DNE
		let pin_channel_id = if let Ok(Some(c)) = self.db.get_channel_id(&guild_id)
		{
			c
		}
		else { return };

		// ignore if already pinned
		let is_pinned = if let Ok(b) = self.db.is_pinned(&guild_id, &add_reaction.message_id)
		{
			b
		}
		else { return };
		if is_pinned
		{ return };

		// fetch message object
		let message = if let Ok(m) = get_message_from_id(&ctx.http, &add_reaction.channel_id, &add_reaction.message_id).await
		{
			m
		}
		else { return };

		if get_reaction_count(REACTION_EMOJI, &message).await < THRESHOLD
		{ return };

		let user = &message.author;

		let user_info = ctx.cache.current_user().await;
		if user.id == user_info.id
		{ return };

		// get data from async methods here, due to async closure constraints
		let name = match user.nick_in(&ctx.http, guild_id).await
		{
			Some(s) => s,
			_ => user.name.clone()
		};
		let color = get_user_color(&ctx, guild_id, &user).await.unwrap_or(Color::RED);
		let content = message.content_safe(&ctx.cache).await;

		if let Err(e) = pin_channel_id.send_message(&ctx.http, |m|
		{
			m.content(MessageBuilder::new()
				.push(format!("@{}#{}: {}\n", &user.name, &user.discriminator, user.id.as_u64()))
				.mention(&add_reaction.channel_id)
				.push(format!(": {}", &add_reaction.channel_id.as_u64()))
				.build()
			);
			m.embed(|e|
			{
				let avatar_url = match user.avatar_url()
				{
					Some(s) => s,
					_ => user.default_avatar_url()
				};
				e.author(|a|
				{
					a.icon_url(&avatar_url);
					a.name(name)
				});

				e.color(color);

				if !content.is_empty()
				{
					e.description(content);
				}

				e.field(
					"Original",
					format!("[Jump!](https://discord.com/channels/{}/{}/{})", guild_id.as_u64(), &add_reaction.channel_id.as_u64(), &add_reaction.message_id.as_u64()),
					false
				);

				if let Some(a) = message.attachments.get(0)
				{
					if let Some(_) = a.height
					{
						e.image(&a.url);
					}
				}

				let timestamp = if let Some(t) = &message.edited_timestamp
				{
					t
				}
				else
				{
					&message.timestamp
				};
				e.timestamp(timestamp);
				
				e
			});
			m
		}).await
		{
			eprintln!("THIS IS NOT A CRASH: An error has occured while sending pin embed to pin channel.");
			eprintln!("Debug info: {:?}", e);
		}
		else // if pinning succeeds
		{
			self.db.create_pin(&guild_id, &add_reaction.message_id).ok();
		}
	}

	async fn ready(&self, ctx: Context, ready: Ready)
	{
		if let Some(s) = &self.motd
		{
			ctx.set_activity(Activity::playing(s)).await;
		}
		println!("Logged in as {}#{}!", &ready.user.name, &ready.user.discriminator);
		println!(
			"Use the following link to invite the bot: https://discord.com/oauth2/authorize?client_id={}&scope=bot&permissions={}", 
			&ready.user.id,
			"314432"
		);
	}	
}

async fn find_default_writable_channel_id(ctx: &Context, guild: &Guild) -> Option<ChannelId>
{
	// get list of channels
	match guild.channels(&ctx.http).await
	{
		Ok(guild_channel_map) =>
		{
			// get "original" default channel
			let channel_id = ChannelId::from(*guild.id.as_u64());
			if let Some(channel) = guild_channel_map.get(&channel_id)
			{
				if let Ok(true) = is_channel_writable(&ctx, channel).await
				{
					return Some(channel_id);
				}
			}

			// check for a channel that has name "general"
			if let Some(channel_id) = guild.channel_id_from_name(&ctx.cache, "general").await
			{
				if let Some(channel) = guild_channel_map.get(&channel_id)
				{
					if let Ok(true) = is_channel_writable(&ctx, channel).await
					{
						return Some(channel_id);
					}
				}
			}

			// if all else fails, fist channel (in order) where the bot can speak
			let mut channels = guild_channel_map.into_iter()
				.filter(|(_, v)| v.kind == ChannelType::Text)
				.collect::<Vec<(ChannelId, GuildChannel)>>();
			
			// sort by position
			channels.sort_by(|(_, channel_one), (_, channel_two)|
			{
				if let Some(o) = channel_two.position.partial_cmp(&channel_one.position)
				{
					return o;
				}
				return Ordering::Equal;
			});

			for (id, channel) in channels
			{
				if let Ok(true) = is_channel_writable(&ctx, &channel).await
				{
					return Some(id);
				}
			}
			
			None
		},
		Err(e) =>
		{
			eprintln!("THIS IS NOT A CRASH: An error has occured while getting guild's channels.");
			eprintln!("Guild name: {}, Guild ID: {}, Debug info: {:?}", guild.name, guild.id, e);
			None
		}
	}
}

// TODO: overload with GuildChannel and Channel
// TODO: anonymous sum types?
async fn is_channel_writable(ctx: &Context, channel: &GuildChannel) -> Result<bool, Box<dyn Error>>
{
	let user_id = ctx.cache.current_user_id().await;
	Ok(channel.permissions_for_user(&ctx.cache, user_id).await?.send_messages())
}

async fn get_message_from_id(http: &Http, channel_id: &ChannelId, message_id: &MessageId) -> Result<Message, SerenityError>
{
	match http.get_message(*channel_id.as_u64(), *message_id.as_u64()).await
	{
		Ok(m) =>
		{
			Ok(m)
		}
		Err(e) =>
		{
			eprintln!("THIS IS NOT A CRASH: An error has occured while coverting MessageId to Message object.");
			eprintln!("Message ID: {}, Channel ID: {}, Debug info: {:?}", message_id, channel_id, e);
			Err(e)
		}
	}
}

// TODO: ignore counts done by bots
async fn get_reaction_count(emoji: &str, message: &Message) -> u64
{
	for r in &message.reactions
	{
		if let ReactionType::Unicode(s) = &r.reaction_type
		{
			if s.eq(emoji)
			{
				return r.count;
			}
		}
	}

	0
}

async fn get_user_color(ctx: &Context, guild_id: GuildId, user: &User) -> Option<Color>
{
	let guild = match guild_id.to_guild_cached(&ctx.cache).await
	{
		Some(g) => g,
		_ => return None
	};

	let mut roles = guild.roles.clone().into_iter().collect::<Vec<(RoleId, Role)>>();
	roles.sort_by(|(_, role_one), (_, role_two)|
	{
		if let Some(o) = role_two.position.partial_cmp(&role_one.position)
		{
			return o;
		}
		return Ordering::Equal;
	});

	for (role_id, role) in roles
	{
		let match_found = match user.has_role(&ctx.http, guild_id, role_id).await
		{
			Ok(b) => b,
			_ => return None
		};

		if match_found
		{
			return Some(role.colour);
		}
	}

	None
}
